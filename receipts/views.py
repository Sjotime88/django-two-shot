
from django.views.generic import ListView, CreateView  
from receipts.models import Receipt 
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect 
 


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt 
    template_name = "receipts/list.html"
    context_object_name = 'receipts' 
    
    #def get_context_data(self, **kwargs): 
        #context = super().get_context_data(**kwargs)
        #context["title"] = "Receipts"
        #return context   (this allows to grab mroe context and modify it before sending to the screen)

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)
    #Change the queryset of the view to filter the Receipt objects where purchaser equals the logged in user


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"] 
    

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user 
        item.save()
        return redirect("home")


    #from 2 shot reference guide on Create views 
    ## Redirects to the detail page for the
    # instance just created, assuming that
    # the path name is registered
    #def get_success_url(self):
        #return reverse_lazy("model_name_detail", args=[self.#object.id])