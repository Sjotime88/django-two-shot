
#THIS IS THE APP - RECEIPTS: URLS.PY FILE 

from django.urls import path 
 
#from django.contrib.auth.views import 

from receipts.views import ReceiptListView, ReceiptCreateView



urlpatterns = [

path ('', ReceiptListView.as_view(), name = "home"),


#Register that RceiptCreateView for the path "create/" in the receipts urls.py and the name "create_receipt"
path ("create/", ReceiptCreateView.as_view(), name = "create_receipt")

]
