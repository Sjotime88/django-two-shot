

#THIS IS THE APP - ACCOUNTS: VIEWS.PY FILE

from django.contrib.auth.forms import UserCreationForm  #You'll need to import the UserCreationForm from the built-in auth forms 

from django.shortcuts import render, redirect  #imported to be able to render and redirect below. Both render and redirect must be returned or nothing will be sent back to the browser. 

from django.contrib.auth.models import User #need to get someones username and password 

from django.contrib.auth import login #to allow user logins 


#You'll need to use the special create_user  method to create a new user account from their username and password
def signup(request):  #request is the incoming browser request. Need to wire it up in URLs.py of App  
    
    if request.method == 'POST':
        print(request.method)
        print(request.post)
        formvar = UserCreationForm(request.POST) #process the data from the form 
        if formvar.is_valid(): #validating signup credentials 
            user = formvar.save #this will save the user to the DB
            login(request, user) #this will login that saved user 
            return redirect("home")  #After you have created the user, redirect the browser to the path registered with the name "home" 
    else:     
        formvar = UserCreationForm()#show the blank form 
            
    return render(request, "registration/signup.html", {"form": formvar})  #this is the 'context' calling form.as_p inside signup.html form (.as_p just returns a bunch of html). This is a dictionary with "form" as key and formvar as value. The key "form" becomes a variable in the signup.html file holding an instance of the UserCreationForm - formvar 





