

#THIS IS THE APP - ACCOUNTS: URLS.PY FILE


from django.urls import path 

 
from django.contrib.auth.views import LoginView, LogoutView #from django.contrib.auth import views as auth_views   


from accounts.views import signup #this is the view in views.py (from App - Accounts: Views.PY. Now add the 'signup' path below...)

urlpatterns = [

path ("login/", LoginView.as_view(),  name = "login"),
path ("logout/", LogoutView.as_view(), name = "logout"), 

path ('signup/', signup, name="signup"),   # Now its wired up from App -Accounts: Views.py  (signup is the name of the FUNCTIONAL View on that page)



]



